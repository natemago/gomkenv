#!/bin/bash

read -d '' ACTIVATE_SCRIPT << EOT
_GOENV_ENABLED=""

_ORIG_GOPATH="\$GOPATH"
_ORIG_GOBIN="\$GOBIN"
_ORIG_PATH="\$PATH"
_ORIG_PS1="\$PS1"

function enable() {
        env_dir="\$1"
        env_name=\`basename "\$env_dir"\`
        if [ \$_GOENV_ENABLED ]; then
                echo "Already enabled"
                return
        fi
        export GOPATH="\$env_dir:\$GOPATH"
        export GOBIN="\$env_dir/bin"
        export PATH="\$GOBIN:\$PATH"
        echo "\$env_name" > "\${env_dir}/__goenv"
        export PS1="\\\\e[36m(\${env_name})\\\\e[0m\${PS1}"
        export _GOENV_ENABLED="true"
}


function deactivate(){
        if [ ! \$_GOENV_ENABLED ]; then
                return
        fi

        export GOPATH="\$_ORIG_GOPATH"
        export GOBIN="\$_ORIG_GOBIN"
        export PATH="\$_ORIG_PATH"
        export PS1="\$_ORIG_PS1"
        export _GOENV_ENABLED=""
}

EOT



function gomkenv() {
	target_dir="$1"
	target_dir=`realpath "$target_dir"`
	env_name=`basename "$target_dir"`
	if [ -f "$target_dir/__goenv" ]; then
		echo "Go env already exists. Please use '${target_dir}/activate' to activate the env"
		return
	fi
	mkdir -p "$target_dir/src"
	mkdir -p "$target_dir/bin"
	mkdir -p "$target_dir/pkg"
	echo "$ACTIVATE_SCRIPT" > "$target_dir/activate"	
	echo "enable \"${target_dir}\"" >> "$target_dir/activate"
        chmod +x "$target_dir/activate"
	echo  "$env_name" > "$target_dir/__goenv"
	echo "Go env ${env_name} created. Please source $target_dir/activate to activated it."
}



